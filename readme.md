1. create network
```console
docker network create mern-webapp-network
```

2. run envoy
```console
docker run -d -p 80:80 -p 9901:9901 \
    --network mern-webapp-network --name envoy-mern \
    -v /envoy:/etc/envoy \
    envoyproxy/envoy-alpine:v1.16.0
```

3. run mongo
```console
docker run -d --network mern-webapp-network --name mongo \
    -v mongo:/data/db \
    mongo
```

4. deploy backend. [**Visit the backend repos here**](https://gitlab.com/ezaaputra/mern-webapp-api)<br>

5. deploy frontend. [**Visit the frontend repos here**](https://gitlab.com/ezaaputra/mern-webapp-client)<br>
